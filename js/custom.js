const initRevolution = () => {
  if ($.isFunction($.fn['themePluginRevolutionSlider'])) {

		$(function() {
			$('[data-plugin-revolution-slider]:not(.manual), .slider-container .slider:not(.manual)').each(function() {
				var $this = $(this),
					opts;

				var pluginOptions = theme.fn.getOptions($this.data('plugin-options'));
				if (pluginOptions)
					opts = pluginOptions;

				$this.themePluginRevolutionSlider(opts);
			});
		});
	}
}


$(document).ready(function () {
  i18next
    .use(i18nextXHRBackend)
    .use(i18nextBrowserLanguageDetector)
    .init(
      {
        lng: 'pt-BR', //navigator.language,
        debug: false,
        fallbackLng: 'en',
        backend: {
          loadPath: '/i18n/{{ns}}/{{lng}}.json',
          crossDomain: true
        }
      },
      function (err, t) {
        let flag
        switch (navigator.language) {
          case 'pt':
            flag = 'pt'
            break
          case 'pt-BR':
            flag = 'br'
            break
          case 'en':
            flag = 'us'
            break
          case 'en-US':
            flag = 'us'
            break
          default:
            lang = 'en'
            console.warn('lang not supported', navigator.language)
        }

        document.getElementById('chosen-language').className = `flag flag-${flag}`
        jqueryI18next.init(i18next, $)
        $(document).localize()
        initRevolution()
      }
    )
})

const changeLanguage = lang => {
  let flag
  switch (lang) {
    case 'pt':
      flag = 'pt'
      break
    case 'pt-br':
      flag = 'br'
      break
    case 'en':
      flag = 'us'
      break
  }
  document.getElementById('chosen-language').className = `flag flag-${flag}`
  i18next.changeLanguage(lang).then(t => $(document).localize())
}

$('#language-list').delegate('li', 'click', evt => changeLanguage($(evt.target).attr('i18n')))
